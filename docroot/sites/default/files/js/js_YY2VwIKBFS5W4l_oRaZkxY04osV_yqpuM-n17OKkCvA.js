(function ($) {
Drupal.behaviors.lang_dropdown = {
  attach: function (context, settings) {
    var settings = settings || Drupal.settings;

    if (settings.lang_dropdown) {
      var flags, msddSettings;
      for (key in settings.lang_dropdown) {
        msddSettings = settings.lang_dropdown[key].jsWidget;
        flags = msddSettings.languageicons;
        if (flags) {
          $.each(flags, function(index, value) {
            if (msddSettings.widget == "msdropdown") {
              $('select#lang-dropdown-select-' + key + ' option[value="' + index + '"]').attr('data-image', value);
            }
            else if (msddSettings.widget == "ddslick" && Boolean(msddSettings.showSelectedHTML)) {
              $('select#lang-dropdown-select-' + key + ' option[value="' + index + '"]').attr('data-imagesrc', value);
            }
          });
        }

        if (msddSettings.widget == "msdropdown") {
          try {
            $('select#lang-dropdown-select-' + key).msDropDown({
              visibleRows: msddSettings.visibleRows,
              roundedCorner: Boolean(msddSettings.roundedCorner),
              animStyle: msddSettings.animStyle,
              event: msddSettings.event,
              mainCSS: msddSettings.mainCSS
            });
          }
          catch (e) {
            if (console) { console.log(e); }
          }
        }
        else if (msddSettings.widget == "chosen") {
          $('select#lang-dropdown-select-' + key).chosen({
            disable_search: msddSettings.disable_search,
            no_results_text: msddSettings.no_results_text
          });
        }
        else if (msddSettings.widget == "ddslick") {
          $.data(document.body, 'ddslick'+key+'flag', 0);
          $('select#lang-dropdown-select-' + key).ddslick({
            width: msddSettings.width,
            height: (msddSettings.height == 0) ? null : msddSettings.height,
            showSelectedHTML: Boolean(msddSettings.showSelectedHTML),
            imagePosition: msddSettings.imagePosition,
            onSelected: function(data) {
              var i = $.data(document.body, 'ddslick'+key+'flag');
              if (i) {
                $.data(document.body, 'ddslick'+key+'flag', 0);
                var lang = data.selectedData.value;
                var href = $('#lang-dropdown-select-'+key).parents('form').find('input[name="' + lang + '"]').val();
                window.location.href = href;
              }
              $.data(document.body, 'ddslick'+key+'flag', 1);
            }
          });
        }
      }
    }

    $('select.lang-dropdown-select-element').change(function() {
      var lang = this.options[this.selectedIndex].value;
      var href = $(this).parents('form').find('input[name="' + lang + '"]').val();
      window.location.href = href;
    });

    $('form.lang-dropdown-form').after('<div style="clear:both;"></div>');
  }
};
})(jQuery);
;
(function ($) {

Drupal.toolbar = Drupal.toolbar || {};

/**
 * Attach toggling behavior and notify the overlay of the toolbar.
 */
Drupal.behaviors.toolbar = {
  attach: function(context) {

    // Set the initial state of the toolbar.
    $('#toolbar', context).once('toolbar', Drupal.toolbar.init);

    // Toggling toolbar drawer.
    $('#toolbar a.toggle', context).once('toolbar-toggle').click(function(e) {
      Drupal.toolbar.toggle();
      // Allow resize event handlers to recalculate sizes/positions.
      $(window).triggerHandler('resize');
      return false;
    });
  }
};

/**
 * Retrieve last saved cookie settings and set up the initial toolbar state.
 */
Drupal.toolbar.init = function() {
  // Retrieve the collapsed status from a stored cookie.
  var collapsed = $.cookie('Drupal.toolbar.collapsed');

  // Expand or collapse the toolbar based on the cookie value.
  if (collapsed == 1) {
    Drupal.toolbar.collapse();
  }
  else {
    Drupal.toolbar.expand();
  }
};

/**
 * Collapse the toolbar.
 */
Drupal.toolbar.collapse = function() {
  var toggle_text = Drupal.t('Show shortcuts');
  $('#toolbar div.toolbar-drawer').addClass('collapsed');
  $('#toolbar a.toggle')
    .removeClass('toggle-active')
    .attr('title',  toggle_text)
    .html(toggle_text);
  $('body').removeClass('toolbar-drawer').css('paddingTop', Drupal.toolbar.height());
  $.cookie(
    'Drupal.toolbar.collapsed',
    1,
    {
      path: Drupal.settings.basePath,
      // The cookie should "never" expire.
      expires: 36500
    }
  );
};

/**
 * Expand the toolbar.
 */
Drupal.toolbar.expand = function() {
  var toggle_text = Drupal.t('Hide shortcuts');
  $('#toolbar div.toolbar-drawer').removeClass('collapsed');
  $('#toolbar a.toggle')
    .addClass('toggle-active')
    .attr('title',  toggle_text)
    .html(toggle_text);
  $('body').addClass('toolbar-drawer').css('paddingTop', Drupal.toolbar.height());
  $.cookie(
    'Drupal.toolbar.collapsed',
    0,
    {
      path: Drupal.settings.basePath,
      // The cookie should "never" expire.
      expires: 36500
    }
  );
};

/**
 * Toggle the toolbar.
 */
Drupal.toolbar.toggle = function() {
  if ($('#toolbar div.toolbar-drawer').hasClass('collapsed')) {
    Drupal.toolbar.expand();
  }
  else {
    Drupal.toolbar.collapse();
  }
};

Drupal.toolbar.height = function() {
  var $toolbar = $('#toolbar');
  var height = $toolbar.outerHeight();
  // In modern browsers (including IE9), when box-shadow is defined, use the
  // normal height.
  var cssBoxShadowValue = $toolbar.css('box-shadow');
  var boxShadow = (typeof cssBoxShadowValue !== 'undefined' && cssBoxShadowValue !== 'none');
  // In IE8 and below, we use the shadow filter to apply box-shadow styles to
  // the toolbar. It adds some extra height that we need to remove.
  if (!boxShadow && /DXImageTransform\.Microsoft\.Shadow/.test($toolbar.css('filter'))) {
    height -= $toolbar[0].filters.item("DXImageTransform.Microsoft.Shadow").strength;
  }
  return height;
};

})(jQuery);
;
