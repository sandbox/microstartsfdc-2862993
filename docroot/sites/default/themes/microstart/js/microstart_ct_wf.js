/**
 * @file microstart_ct_wf.js
 * 
 */

(function($) {
  $(document).ready(function() {
    /*
     * Web-form Field erasing default value.
     * 
     */
    $(".form-text").focus(cleanDefaultValue);
    $(".form-textarea").focus(cleanDefaultValue);
    $(".form-text").blur(retrieveDefaultValue);
    $(".form-textarea").blur(retrieveDefaultValue);
    function cleanDefaultValue() {
      if (this.value === this.defaultValue) {
        this.value = '';
      }
    }  
    function retrieveDefaultValue() {
      if (this.value === '') {
        this.value = this.defaultValue;
      }
    }
  });
})(jQuery);