<?php
/**
 * @file
 * Default theme implementation to display a node.
 *
 * Available variables:
 * - $title: the (sanitized) title of the node.
 * - $content: An array of node items. Use render($content) to print them all,
 *   or print a subset such as render($content['field_example']). Use
 *   hide($content['field_example']) to temporarily suppress the printing of a
 *   given element.
 * - $user_picture: The node author's picture from user-picture.tpl.php.
 * - $date: Formatted creation date. Preprocess functions can reformat it by
 *   calling format_date() with the desired parameters on the $created variable.
 * - $name: Themed username of node author output from theme_username().
 * - $node_url: Direct URL of the current node.
 * - $display_submitted: Whether submission information should be displayed.
 * - $submitted: Submission information created from $name and $date during
 *   template_preprocess_node().
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. The default values can be one or more of the
 *   following:
 *   - node: The current template type; for example, "theming hook".
 *   - node-[type]: The current node type. For example, if the node is a
 *     "Blog entry" it would result in "node-blog". Note that the machine
 *     name will often be in a short form of the human readable label.
 *   - node-teaser: Nodes in teaser form.
 *   - node-preview: Nodes in preview mode.
 *   The following are controlled through the node publishing options.
 *   - node-promoted: Nodes promoted to the front page.
 *   - node-sticky: Nodes ordered above other non-sticky nodes in teaser
 *     listings.
 *   - node-unpublished: Unpublished nodes visible only to administrators.
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 *
 * Other variables:
 * - $node: Full node object. Contains data that may not be safe.
 * - $type: Node type; for example, story, page, blog, etc.
 * - $comment_count: Number of comments attached to the node.
 * - $uid: User ID of the node author.
 * - $created: Time the node was published formatted in Unix timestamp.
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 * - $zebra: Outputs either "even" or "odd". Useful for zebra striping in
 *   teaser listings.
 * - $id: Position of the node. Increments each time it's output.
 *
 * Node status variables:
 * - $view_mode: View mode; for example, "full", "teaser".
 * - $teaser: Flag for the teaser state (shortcut for $view_mode == 'teaser').
 * - $page: Flag for the full page state.
 * - $promote: Flag for front page promotion state.
 * - $sticky: Flags for sticky post setting.
 * - $status: Flag for published status.
 * - $comment: State of comment settings for the node.
 * - $readmore: Flags true if the teaser content of the node cannot hold the
 *   main body content.
 * - $is_front: Flags true when presented in the front page.
 * - $logged_in: Flags true when the current user is a logged-in member.
 * - $is_admin: Flags true when the current user is an administrator.
 *
 * Field variables: for each field instance attached to the node a corresponding
 * variable is defined; for example, $node->body becomes $body. When needing to
 * access a field's raw values, developers/themers are strongly encouraged to
 * use these variables. Otherwise they will have to explicitly specify the
 * desired field language; for example, $node->body['en'], thus overriding any
 * language negotiation rule that was previously applied.
 *
 * @see template_preprocess()
 * @see template_preprocess_node()
 * @see template_process()
 *
 * @ingroup templates
 */
?>
<article id="node-<?php print $node->nid; ?>"
  class="<?php print $classes; ?> clearfix" <?php print $attributes; ?>>
  <?php if ((!$page && !empty($title)) || !empty($title_prefix) || !empty($title_suffix) || $display_submitted): ?>
  <header>
    <?php print render($title_prefix); ?>
    <?php if (!$page && !empty($title)): ?>
    <h2 <?php print $title_attributes; ?>>
      <a href="<?php print $node_url; ?>"><?php print $title; ?></a>
    </h2>
    <?php endif; ?>
    <?php print render($title_suffix); ?>
    <?php if ($display_submitted): ?>
    <span class="submitted">
      <?php print $user_picture; ?>
      <?php print $submitted; ?>
    </span>
    <?php endif; ?>
  </header>
  <?php endif; ?>
  <?php
  // Hide comments, tags, and links now so that we can render them later.
  hide ( $content ['comments'] );
  hide ( $content ['links'] );
  hide ( $content ['field_tags'] );
  hide ( $content ['field_cred_next_term_date_text'] );
  hide ( $content ['field_credit_amount_delay_text'] );
  hide ( $content ['field_cred_remaining_cost_text'] );
  hide ( $content ['field_cred_choosen_amount_text'] );
  hide ( $content ['field_image_pay'] );
  hide ( $content ['field_link_pay'] );
  ?>
  <?php $credit = microstart_saleforce_credit_management($_GET["cred_ref"]); ?>
  <?php $credit_general_info = $credit['general_info']; ?>
  <?php $credit_count = $credit['count']; ?>
  <?php $credit_timetable_initial = $credit['timetable_initial']; ?>
  <div class="ct_c_credit_management_payement">
  <div class="panel panel-default panel-microstart-back center-block">
  <div class="panel-body">

    <div class="row row-centered" style="padding-bottom:10px;">
      <div class="col-xs-12 col-centered-centered">
        <div class="color-primary"><h1><?php print $credit_general_info['cred_ref']; ?></h1></div>
      </div><!-- /.col -->
    </div><!-- /.row -->

       <div class="row row-centered">
       <div class="col-xs-12 col-sm-6 col-md-6 col-lg-4 col-centered">
        <div class="panel panel-default">
          <div class="panel-body">


            <div style="clear:both;">
              <div style="float:left">
                <input type="radio" name="payement_type" value="next_term" checked="checked">
                <div style="display:inline-block;"><?php print render($content['field_cred_next_term_date_text']); ?></div> <b style="display:inline-block;"><?php print $credit_general_info['cred_next_term_date']; ?></b>
              </div>
              <div style="float:right">
                <div style="color:green;"><?php print $credit_general_info['cred_term_amount']; ?> €</div>
              </div>
            </div>

            <div style="clear:both;">
              <div style="float:left">
                <input type="radio" name="payement_type" value="delay">
                <div style="display:inline-block;"><?php print render($content['field_credit_amount_delay_text']); ?></div>
              </div>
              <div style="float:right">
                <div style="color:red;"><?php print ($credit_general_info['cred_term_amount'] * $credit_general_info['cred_qty_term_delayed']) ?>  €</div>
              </div>
            </div>

            <div style="clear:both;">
              <div style="float:left">
                <input type="radio" name="payement_type" value="full">
                <div style="display:inline-block;"><?php print render($content['field_cred_remaining_cost_text']); ?></div>
              </div>
              <div style="float:right">
                <div class="color-primary"><?php print ($credit_general_info['cred_initial_cost'] - $credit_general_info['cred_capital_reimbursed']); ?>  €</div>
              </div>
            </div>

            <div style="clear:both;padding-bottom:50px;">
              <div style="float:left">
                <input type="radio" name="payement_type" value="choosen"></span>
                <div style="display:inline-block;"><?php print render($content['field_cred_choosen_amount_text']); ?></div>
              </div>
              <div style="float:right">
                <input id="pay_amount" type="number" min="0" max="<?php print ($credit_general_info['cred_initial_cost'] - $credit_general_info['cred_capital_reimbursed']); ?>" step="100" value="0">
              </div>
            </div>


<div class="row row-centered visible-md-block visible-lg-block" style="padding-bottom:10px;">
<div class="col-xs-12 col-md-11 col-centered col-top">
<?php print '<div class="microstart-button microstart-button-secondary microstart-button-font-size"><a role="button" onclick="goToPay()">'; ?>
<div class="panel panel-default center-block button-sharp background-color-secondary">
<div class="panel-body" style="padding:0;">
<div style="display:table;width:100%;">
<?php $content['field_image_pay'][0]['#item']['attributes']['class'][] = "img-circle"; ?>
<?php $content['field_image_pay'][0]['#item']['attributes']['style'][] = "width:100px;heigth:100px;"; ?>
<div style="display:inline-block;vertical-align:middle;display:table-cell;"><?php print render($content['field_image_pay']); ?></div>
<div style="display:inline-block;vertical-align:middle;display:table-cell;"><?php print render($content['field_link_pay']); ?></div>
</div>
</div><!-- /.panel-body -->
</div><!-- /.panel -->
</a></div>
</div><!-- /.col -->
</div><!-- /.row -->


<div class="row row-centered visible-xs-block visible-sm-block visible-md-block">
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-3 col-centered-centered" style="font-size:200%;">
<?php print '<div class="microstart-button-secondary" style="padding-bottom:10px;"><a role="button" onclick="goToPay()">'; ?>
<?php print '<div class="panel panel-default" style="background-color:#FF9919;"><div class="panel-body">' . render($content['field_link_pay']) . '</div><!-- /.panel-body --></div><!-- /.panel -->'; ?>
<?php print '</a></div>'; ?>
</div><!-- /.col -->
</div><!-- /.row -->


          </div><!-- /.panel-body -->
        </div><!-- /.panel -->
      </div><!-- /.col -->
      </div><!-- /.row -->

  </div><!-- /.panel-body -->
  </div><!-- /.panel -->

  </div>
  <!-- /.ct_c_credit_management_payement -->
  <?php print render($content); ?>
  <?php if (!empty($content['field_tags']) || !empty($content['links'])): ?>
  <footer>
    <?php print render($content['field_tags']); ?>
    <?php print render($content['links']); ?>
  </footer>
  <?php endif; ?>
  <?php print render($content['comments']); ?>
</article>
<script type="text/javascript">
function goToPay() {
(function ($) {
  var pay_amount = 0;
  if ($('input[name=payement_type]:checked').val() == "next_term") {
      var pay_amount = <?php print $credit_general_info['cred_term_amount']; ?>;
  } else if ($('input[name=payement_type]:checked').val() == "delay") {
      var pay_amount = <?php print ($credit_general_info['cred_term_amount'] * $credit_general_info['cred_qty_term_delayed']) ?>;
  } else if ($('input[name=payement_type]:checked').val() == "full") {
      var pay_amount = <?php print $credit_general_info['cred_capital_reimbursed']; ?>;
  } else {
  	var pay_amount = $('input[id=pay_amount]').val();
  }
  window.location.href = 'credit-management/pay/' + pay_amount;
})(jQuery);
}
</script>
