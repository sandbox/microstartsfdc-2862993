<?php
/**
 * @file
 * Default theme implementation to display a node.
 *
 * Available variables:
 * - $title: the (sanitized) title of the node.
 * - $content: An array of node items. Use render($content) to print them all,
 *   or print a subset such as render($content['field_example']). Use
 *   hide($content['field_example']) to temporarily suppress the printing of a
 *   given element.
 * - $user_picture: The node author's picture from user-picture.tpl.php.
 * - $date: Formatted creation date. Preprocess functions can reformat it by
 *   calling format_date() with the desired parameters on the $created variable.
 * - $name: Themed username of node author output from theme_username().
 * - $node_url: Direct URL of the current node.
 * - $display_submitted: Whether submission information should be displayed.
 * - $submitted: Submission information created from $name and $date during
 *   template_preprocess_node().
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. The default values can be one or more of the
 *   following:
 *   - node: The current template type; for example, "theming hook".
 *   - node-[type]: The current node type. For example, if the node is a
 *     "Blog entry" it would result in "node-blog". Note that the machine
 *     name will often be in a short form of the human readable label.
 *   - node-teaser: Nodes in teaser form.
 *   - node-preview: Nodes in preview mode.
 *   The following are controlled through the node publishing options.
 *   - node-promoted: Nodes promoted to the front page.
 *   - node-sticky: Nodes ordered above other non-sticky nodes in teaser
 *     listings.
 *   - node-unpublished: Unpublished nodes visible only to administrators.
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 *
 * Other variables:
 * - $node: Full node object. Contains data that may not be safe.
 * - $type: Node type; for example, story, page, blog, etc.
 * - $comment_count: Number of comments attached to the node.
 * - $uid: User ID of the node author.
 * - $created: Time the node was published formatted in Unix timestamp.
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 * - $zebra: Outputs either "even" or "odd". Useful for zebra striping in
 *   teaser listings.
 * - $id: Position of the node. Increments each time it's output.
 *
 * Node status variables:
 * - $view_mode: View mode; for example, "full", "teaser".
 * - $teaser: Flag for the teaser state (shortcut for $view_mode == 'teaser').
 * - $page: Flag for the full page state.
 * - $promote: Flag for front page promotion state.
 * - $sticky: Flags for sticky post setting.
 * - $status: Flag for published status.
 * - $comment: State of comment settings for the node.
 * - $readmore: Flags true if the teaser content of the node cannot hold the
 *   main body content.
 * - $is_front: Flags true when presented in the front page.
 * - $logged_in: Flags true when the current user is a logged-in member.
 * - $is_admin: Flags true when the current user is an administrator.
 *
 * Field variables: for each field instance attached to the node a corresponding
 * variable is defined; for example, $node->body becomes $body. When needing to
 * access a field's raw values, developers/themers are strongly encouraged to
 * use these variables. Otherwise they will have to explicitly specify the
 * desired field language; for example, $node->body['en'], thus overriding any
 * language negotiation rule that was previously applied.
 *
 * @see template_preprocess()
 * @see template_preprocess_node()
 * @see template_process()
 *
 * @ingroup templates
 */
?>
<article id="node-<?php print $node->nid; ?>"
  class="<?php print $classes; ?> clearfix" <?php print $attributes; ?>>
  <?php if ((!$page && !empty($title)) || !empty($title_prefix) || !empty($title_suffix) || $display_submitted): ?>
  <header>
    <?php print render($title_prefix); ?>
    <?php if (!$page && !empty($title)): ?>
    <h2 <?php print $title_attributes; ?>>
      <a href="<?php print $node_url; ?>"><?php print $title; ?></a>
    </h2>
    <?php endif; ?>
    <?php print render($title_suffix); ?>
    <?php if ($display_submitted): ?>
    <span class="submitted">
      <?php print $user_picture; ?>
      <?php print $submitted; ?>
    </span>
    <?php endif; ?>
  </header>
  <?php endif; ?>
  <?php
  // Hide comments, tags, and links now so that we can render them later.
  hide ( $content ['comments'] );
  hide ( $content ['links'] );
  hide ( $content ['field_tags'] );
  hide ( $content ['field_contact_microstart_image'] );
  hide ( $content ['field_contact_microstart_text'] );
  hide ( $content ['field_credit_list'] );
  hide ( $content ['field_credit_bp_image'] );
  hide ( $content ['field_credit_approved'] );
  hide ( $content ['field_credit_progress'] );
  hide ( $content ['field_credit_refused'] );
  hide ( $content ['field_details'] );
  hide ( $content ['field_news'] );
  hide ( $content ['field_news_content'] );
  hide ( $content ['field_documents'] );
  hide ( $content ['field_title'] );
  hide ( $content ['field_description'] );
  hide ( $content ['field_date'] );
  hide ( $content ['contact_microstart_image'] );
  hide ( $content ['contact_microstart_text'] );
  hide ( $content ['field_document'] );
  hide ( $content ['contact_document_desc'] );
  hide ( $content ['contact_document_date'] );
  ?>
  <?php // $prospect_list = microstart_saleforce_prospect_list(); ?>
  <div class="ct_c_credit_management_list">
  <div class="panel panel-default panel-microstart-back center-block">
  <div class="panel-body">

<div class="row row-centered">

<?php // print '<div class="col-xs-12 col-sm-12 col-md-12 col-lg-3 col-top" style="padding-bottom:10px;font-size:200%;">';  ?>
<?php // $content['field_contact_microstart_image'][0]['#item']['attributes']['class'][] = "center-block img-circle"; ?>
<?php // $content['field_contact_microstart_image'][0]['#item']['attributes']['style'][] = "width:220px;heigth:220px;border: 1px solid #0066CC;"; ?>
<?php // print '<div style="padding-bottom:10px;">' . render($content['field_contact_microstart_image']) . '</div>'; ?>
<?php // print '<div class="microstart-button" style="padding-bottom:10px;"><a href="http://microstart.be/fr/contact">'; ?>
<?php // print '<div class="panel panel-default button-sharp background-color-primary"><div class="panel-body">' . render($content['field_contact_microstart_text']) . '</div><!-- /.panel-body --></div><!-- /.panel -->'; ?>
<?php // print '</a></div>'; ?>
<?php // print '</div><!-- /.col -->';  ?>

<?php print '<!-- BLOCK CENTER -->';  ?>
<?php print '<div class="col-xs-12 col-sm-12 col-md-12 col-lg-9 col-centered" style="text-align:left;">';  ?>
<?php print '<!-- BLOCK CENTER -->';  ?>

<?php // print '<div class="color-primary"><h1>' . render($content['field_credit_list']) . '</h1></div>'; ?>

<?php
// $credit_waiting = 0;
// $credit_cancelled = 0;
// $credit_accepted = 0;
// foreach ( $prospect_list as $prospect):
// switch ($prospect['request_status']) {
//   case 1:
//     $credit_waiting++;
//   break;
//   case 2:
//     $credit_cancelled++;
//   break;
//   case 3:
//     $credit_accepted++;
//   break;
// }
// endforeach;
?>

<?php // print '<div class="microstart-font-size" style="padding-bottom:50px;"><!-- CREDIT APPLICATION -->'; ?>
<?php // print '<div style="display:table;"><!-- OUTER TABLE -->'; ?>
<?php // print '<div style="display:inline-block;vertical-align:middle;display:table-cell;"><!-- TABLE INFO -->'; ?>
<?php // print '<div style="display:table;color:green;table-layout:fixed;">'; ?>
<?php // $content['field_credit_bp_image'][0]['#item']['attributes']['style'][] = "width:50px;heigth:50px;"; ?>
<?php // print '<div style="display:inline-block;vertical-align:middle;display:table-cell;width:60px;">' . render($content['field_credit_bp_image']) . '</div>'; ?>
<?php // print '<div style="display:inline-block;vertical-align:middle;display:table-cell;width:200px;">' . render($content['field_credit_approved']) . '</div>'; ?>
<?php // print '<div style="display:inline-block;vertical-align:middle;display:table-cell;width:50px;">' . $credit_accepted . '</div>'; ?>
<?php // print '</div>'; ?>
<?php // print '<div style="display:table;table-layout:fixed;" class="color-secondary">'; ?>
<?php // $content['field_credit_bp_image'][0]['#item']['attributes']['style'][] = "width:50px;heigth:50px;"; ?>
<?php // print '<div style="display:inline-block;vertical-align:middle;display:table-cell;width:60px;">' . render($content['field_credit_bp_image']) . '</div>'; ?>
<?php // print '<div style="display:inline-block;vertical-align:middle;display:table-cell;width:200px;">' . render($content['field_credit_progress']) . '</div>'; ?>
<?php // print '<div style="display:inline-block;vertical-align:middle;display:table-cell;width:50px;">' . $credit_waiting . '</div>'; ?>
<?php // print '</div>'; ?>
<?php // print '<div style="display:table;color:red;table-layout:fixed;">'; ?>
<?php // $content['field_credit_bp_image'][0]['#item']['attributes']['style'][] = "width:50px;heigth:50px;"; ?>
<?php // print '<div style="display:inline-block;vertical-align:middle;display:table-cell;width:60px;">' . render($content['field_credit_bp_image']) . '</div>'; ?>
<?php // print '<div style="display:inline-block;vertical-align:middle;display:table-cell;width:200px;">' . render($content['field_credit_refused']) . '</div>'; ?>
<?php // print '<div style="display:inline-block;vertical-align:middle;display:table-cell;width:50px;">' . $credit_cancelled . '</div>'; ?>
<?php // print '</div>'; ?>
<?php // print '</div><!-- TABLE INFO -->'; ?>
<?php // print '<div style="display:inline-block;vertical-align:middle;display:table-cell;"><!-- TABLE LINK -->'; ?>
<?php // print '<div class="microstart-button" style="padding-bottom:10px;"><a href="/prospect-list">'; ?>
<?php // print '<div class="panel panel-default button-sharp background-color-primary"><div class="panel-body">' . render($content['field_details']) . '</div><!-- /.panel-body --></div><!-- /.panel -->'; ?>
<?php // print '</a></div>'; ?>
<?php // print '</div><!-- TABLE LINK -->'; ?>
<?php // print '</div><!-- OUTER TABLE -->'; ?>
<?php // print '</div><!-- CREDIT APPLICATION -->'; ?>

<div style="padding-bottom:50px;">
<?php print '<div class="color-primary"><h1>' . render($content['field_news']) . '</h1></div>'; ?>
<?php print '<div>' . render($content['field_news_content']) . '</div>'; ?>
</div>

<?php print '<div class="color-primary"><h1>' . render($content['field_documents']) . '</h1></div>'; ?>
<?php print '<div class="panel panel-default"><div class="panel-body"><div class="table-responsive"><table class="table table-condensed">'; ?>
<?php print '<tr>'; ?>
<?php print '<th>' . render($content['field_title']) . '</th>'; ?>
<?php print '<th>' . render($content['field_description']) . '</th>'; ?>
<?php print '<th>' . render($content['field_date']) . '</th>'; ?>
<?php print '</tr>'; ?>
<?php foreach ($content['field_document'] as $key => $value): ?>
<?php if (is_numeric($key)):  ?>
<?php print '<tr>'?>
<?php print '<td class="microstart-button-primary-secondary">' . render($content['field_document'][$key]) . '</td>'; ?>
<?php if (isset($content['field_document_desc'][$key])): ?>
<?php print '<td>' . render($content['field_document_desc'][$key]) . '</td>'; ?>
<?php else: ?>
<?php print '<td>/</td>'; ?>
<?php endif; ?>
<?php if (isset($content['field_document_date'][$key])): ?>
<?php print '<td>' . render($content['field_document_date'][$key]) . '</td>'; ?>
<?php else: ?>
<?php print '<td>/</td>'; ?>
<?php endif; ?>
<?php print '</tr>'?>
<?php endif; ?>
<?php endforeach; ?>
<?php print '</table></div><!-- /.table-responsive --></div><!-- /.panel-body --></div><!-- /.panel -->'; ?>

<!-- BLOCK CENTER -->
</div><!-- /.col -->
<!-- BLOCK CENTER -->

</div><!-- /.row -->

  </div><!-- /.panel-body -->
  </div><!-- /.panel -->
  </div><!-- /.ct_c_credit_management_list -->
  <?php print render($content); ?>
  <?php if (!empty($content['field_tags']) || !empty($content['links'])): ?>
  <footer>
    <?php print render($content['field_tags']); ?>
    <?php print render($content['links']); ?>
  </footer>
  <?php endif; ?>
  <?php print render($content['comments']); ?>
</article>
<script type="text/javascript">
(function($) {
  $(document).ready(function() {
    $('a[href^="http://microstart-portal.be/system/files"]').attr('target','_blank');
  });
})(jQuery);
</script>
